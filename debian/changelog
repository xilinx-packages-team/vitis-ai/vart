vart (2.5-5) unstable; urgency=medium

  * d/patches:
    - Add 0007-Fix-FTBFS-issues-relating-to-gcc-13-Closes-1037882.patch.
      Fix FTBFS issues relating to gcc-13 (Closes: #1037882)
      Thanks to Jonathan Bergh <bergh.jonathan@gmail.com>.
  * d/copyright:
    - Update copyright year
  * d/gbp.conf:
    - Add file.
  * d/control:
    - Bump Standards-Version to 4.7.0.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Mon, 24 Feb 2025 13:32:55 +0900

vart (2.5-4) unstable; urgency=medium

  * Change library name to load to libvart-trace.so.2.
    Add d/patches/0006-libvart-trace.so-Change-library-name-to-load.patch.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Tue, 28 Feb 2023 07:05:34 +0900

vart (2.5-3) unstable; urgency=medium

  * d/libvart2.install: install /etc/vart.conf

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Wed, 11 Jan 2023 13:46:05 +0900

vart (2.5-2) unstable; urgency=medium

  * Drop embeds build time in various binaries. (Closes: #1021792)
    Add 0005-Use-cmake-TIMESTAMP-feature-instead-of-calling-date-.patch.
    Thanks to Vagrant Cascadian <vagrant@reproducible-builds.org>.
  * d/rules: Add -DCMAKE_BUILD_RPATH_USE_ORIGIN=ON to configure option.
    (Closes: #1021793)
    Fix buildid differences triggered by build paths.
    Thanks to Vagrant Cascadian <vagrant@reproducible-builds.org>.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Wed, 19 Oct 2022 14:01:22 +0900

vart (2.5-1) unstable; urgency=low

  [ Punit Agrawal ]
  * Initial release. (Closes: #1017822)

  [ Nobuhiro Iwamatsu ]
  * Update to 2.5.

 -- Nobuhiro Iwamatsu <iwamatsu@debian.org>  Fri, 19 Aug 2022 16:07:43 +0900
